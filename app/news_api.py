import requests


API_ENDPOINT = 'http://content.guardianapis.com/search?q={q}&api-key={key}'
API_KEY = 'test'


def get_stories(search_key):
    url = API_ENDPOINT.format(q=search_key, key=API_KEY)
    response = None

    try:
        response = requests.get(url)
        return response.json().get(u'response').get(u'results')
    except ConnectionError as exc:
        print(exc.__str__())

    return None
