from news_api import *

def process_command_message(message_request):
    if message_request.startswith('news '):
        request = ' '.join(message_request.split(' ')[1:])
        stories = get_stories(request)
        if stories:
            return {
                'status': 'success',
                'result': stories
            }

    return {
        'status': 'error',
        'message': 'Sorry, I do not understand your message. Please use "news"'
                ' command and a keyword for search the latest news by '
                'category. For ex. "news sports".'
    }